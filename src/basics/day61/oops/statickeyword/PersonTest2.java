package basics.day61.oops.statickeyword;

public class PersonTest2 {

	/**
	 * 1. Understand the usage of static block. Add static and non-static variables
	 * above the static block added below and use them inside the static block and
	 * observe the behavior
	 * 
	 * 2. Understand what is the warning shown at line 36 and resolve the same
	 */

	static {
		System.out.println("This is a static block");
	}

	public static void main(String[] args) {
		Person1 p1 = new Person1();
		p1.setName("Bibin");
		p1.setAge(37);

		Person1 p2 = new Person1();
		p2.setName("Ramesh");
		p2.setAge(25);

		Person1.setCountry("US");

		System.out.println("P1 Details");
		System.out.println(p1.getName());
		System.out.println(p1.getAge());
		System.out.println(Person1.getCountry());

		System.out.println("P2 Details");
		System.out.println(p2.getName());
		System.out.println(p2.getAge());
		System.out.println(p1.getCountry());

	}

}
