package basics.day22.datatypes.arrays;

public class IntegerArray1 {

	/**
	 * 1. Initialize an array with 5 elements viz. 10, 20, 30, 40, 50 and print them
	 * 
	 * 2. Print the length of the array
	 * 
	 * 3. Print the sum of all elements in the array
	 * 
	 * 4. Add the same number to each element in the array, so that the array
	 * elements become 20, 40, 60, 80, 100. This logic should be written in a new
	 * private method returns int[], which is invoked from main method. Print these
	 * values in main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		int[] inputNumbers = new int[] { 10, 20, 30, 40, 50 };
		System.out.println(inputNumbers.length);
	}

}
