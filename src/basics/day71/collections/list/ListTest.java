package basics.day71.collections.list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListTest {

	public static void main(String[] args) {

		Calculator calc = new Calculator() {
			@Override
			public void add(int a, int b) {
				System.out.println("Sum is: " + (a + b));

			}
		};

		calc.add(10, 20);

		Person bibin = new Person("Bibin", 37);
		Person ramesh = new Person("Ramesh", 25);
		Person alex = new Person("Alex", 50);

		List<Person> personList = new ArrayList<>();
		personList.add(bibin);
		personList.add(ramesh);
		personList.add(alex);

		System.out.println("Before sorting:");
		System.out.println();

		for (int i = 0; i < personList.size(); i++) {
			System.out.println("Name: " + personList.get(i).getName() + " : " + personList.get(i).getAge());
		}

		System.out.println("After sorting on name:");
		System.out.println();

		sortByName(personList);

		for (int i = 0; i < personList.size(); i++) {
			System.out.println("Name: " + personList.get(i).getName() + " : " + personList.get(i).getAge());
		}

		System.out.println("After sorting on age:");
		System.out.println();

		sortByAge(personList);

		for (int i = 0; i < personList.size(); i++) {
			System.out.println("Name: " + personList.get(i).getName() + " : " + personList.get(i).getAge());
		}
	}

	private static void sortByAge(List<Person> personList) {
		Comparator<Person> sortByAge = new Comparator<>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getAge() - o2.getAge();
			}
		};
		Collections.sort(personList, sortByAge);
	}

	private static void sortByName(List<Person> personList) {
		Comparator<Person> sortByName = new Comparator<>() {
			@Override
			public int compare(Person o1, Person o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};
		Collections.sort(personList, sortByName);
	}

}
