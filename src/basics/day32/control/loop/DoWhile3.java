package basics.day32.control.loop;

public class DoWhile3 {

	/**
	 * 1. Implement a menu driven calculator. The valid inputs are 1 (add), 2
	 * (subtract), 3 (multiply), 4 (divide), 5 (exit) and then two numbers for the
	 * arithmetic operation. Sample menu and user choice are given below
	 * 
	 * Welcome to Calculator
	 * 
	 * Enter 1 for add, 2 for subtract, 3 for multiply, 4 for divide, 5 for exit
	 * 
	 * 1
	 * 
	 * Please enter the first number
	 * 
	 * 10
	 * 
	 * Please enter the second number
	 * 
	 * 20
	 * 
	 * The sum of 10, 20 = 30
	 * 
	 * Enter 1 for add, 2 for subtract, 3 for multiply, 4 for divide, 5 for exit
	 * 
	 * 5
	 * 
	 * Thanks for using calculator, good bye!
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Welcome to Calculator");
		int choice = 5;
		do {
			System.out.println("Enter 1 for add, 2 for subtract, 3 for multiply, 4 for divide, 5 for exit");
		} while (choice != 5);
	}

}
