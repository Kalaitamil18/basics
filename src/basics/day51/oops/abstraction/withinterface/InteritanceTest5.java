package basics.day51.oops.abstraction.withinterface;

public class InteritanceTest5 {

	public static void main(String[] args) {
		/**
		 * 1. Create instances of the two Impl classes and invoke the respective
		 * functionalities (add(), subtract() or multiply(), divide()). Example provided
		 * below
		 */

		AddAndSubtractCalculator1 calcOne = new AddAndSubtractCalculator1Impl3();
		// calcOne.add(10,20);

		/**
		 * 2. Create a new interface FullyFunctionalCalculator implements
		 * AddAndSubtractCalculator1, MultiplyAndDivideCalculator2
		 * 
		 * 3. Create a new Impl class of the above FullyFunctionalCalculator and the
		 * respective functionalities (add(), subtract(), multiply(), divide()).
		 * 
		 * 4. Play around by deleting methods from interfaces and Impl classes and
		 * analyze the behavior
		 */

	}

}
