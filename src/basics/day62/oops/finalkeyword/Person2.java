package basics.day62.oops.finalkeyword;

/**
 * 1. Add final keyword between public and class below and observe what is
 * happening to the child class extending Person2
 */

public class Person2 {
	private String name;
	private int age;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
