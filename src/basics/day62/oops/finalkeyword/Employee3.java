package basics.day62.oops.finalkeyword;

/**
 * 1. Observe the behavior in this class when Person2 is made as final
 *
 */
public class Employee3 extends Person2 {
	private int employeeID;

	public int getEmployeeID() {
		return employeeID;
	}

	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}
}
