package basics.day21.datatypes;

import java.util.Scanner;

public class Calculator1 {

	/**
	 * 1. Similar to the addTwoNumbers() method written below, write
	 * subtractTwoNumers(), multiplyTwoNumers(), divideTwoNumers()
	 * 
	 * 2. Add additional methods to add, subtract, multiply and divide float numbers
	 * 
	 * 3. Add method to display the remainder. For example, if the first number is
	 * 10 and second number is 3, then the output should be 1 (hint: use % operator)
	 * 
	 * 
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		int firstNumber;
		int secondNumber;

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the first number:");
		System.out.println();
		firstNumber = sc.nextInt();

		System.out.println("Enter the second number:");
		System.out.println();
		secondNumber = sc.nextInt();

		sc.close();

		addTwoNumbers(firstNumber, secondNumber);
	}

	private static void addTwoNumbers(int firstNumber, int secondNumber) {
		System.out.println("Sum of " + firstNumber + " and " + secondNumber + " = " + (firstNumber + secondNumber));
	}

}
