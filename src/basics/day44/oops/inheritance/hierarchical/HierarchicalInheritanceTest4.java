package basics.day44.oops.inheritance.hierarchical;

public class HierarchicalInheritanceTest4 {

	/**
	 * 1. Create an object of class Employee2 and set its properties
	 * 
	 * 2. Create an object of class Student3 and set its properties
	 * 
	 * 2. Set the properties from inherited (Person1) classes as well
	 * 
	 * 3. Print all the properties (native + inherited) of Employee2 and Student3
	 * classes
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
