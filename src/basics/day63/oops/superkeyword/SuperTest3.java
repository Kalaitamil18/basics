package basics.day63.oops.superkeyword;

public class SuperTest3 {

	public static void main(String[] args) {
		/**
		 * 1. Observe the behavior with and without super.add() in Calculator1Impl2
		 * class
		 */
		Calculator1 calc = new Calculator1Impl2();
		calc.add(10, 15);

	}

}
