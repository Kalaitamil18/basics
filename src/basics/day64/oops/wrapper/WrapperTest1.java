package basics.day64.oops.wrapper;

public class WrapperTest1 {

	/**
	 * 1. Try out using a couple of methods available in Integer class (similar to
	 * parseInt() used here)
	 * 
	 * 2. In the below code, try giving "five" instead of "5" and observe the
	 * behavior
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Integer i = Integer.parseInt("5");
		System.out.println(i);
	}

}
