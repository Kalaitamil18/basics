package basics.day31.control.decision;

public class IfElse2 {

	/**
	 * 1. Accept a number from console and print if it is equal to 5. If equals,
	 * then print "The input number is equal to 5". If not, then print "The input
	 * number is not equal to number" (use if)
	 * 
	 * 2. Accept a number from console and check if it is greater than 5. If yes,
	 * then print "The input number is greater than 5". Additionally check if the
	 * input number is greater than 10. If yes, then print "The input number is
	 * greater than 10". So, if the input is 15, then the console should print both
	 * the messages (use if-else)
	 * 
	 * 3. Accept a number from console and check if it is between 1 and 10. If yes,
	 * then print "The number is between 1 and 10". Otherwise, if the number is
	 * between 11 and 20, then print "The number is between 11 and 20". Otherwise,
	 * if the number is between 21 and 30, then print "The number is between 21 and
	 * 30". If none of these conditions are matched, then print "The input number is
	 * not between 1 and 30" (use if-else if-else)
	 * 
	 * 4. Accept a string from console and if it contains "Kolkotha" or "Chennai" or
	 * "New Delhi" or "Hyderabad" or "Mumbai", then print a message "The input
	 * string contains <city>". For example, if the input is "I'm from Chennai",
	 * then the output should be "The input string contains Chennai". If the input
	 * string is "I have visited New Delhi and Mumbai", then the output should be
	 * "The input string contains New Delhi and Mumbai"
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
