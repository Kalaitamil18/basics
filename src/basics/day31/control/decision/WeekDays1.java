package basics.day31.control.decision;

public class WeekDays1 {

	/**
	 * 1. Use switch-case. Accept a number from console and print the day
	 * corresponding to the input number. For example, if the input is 3, then the
	 * output should be Tuesday. Display "Invalid Entry" if user provides an invalid
	 * input; for example 10
	 * 
	 * 2. Implement the above with String instead of number. Valid inputs are "one",
	 * "TWO", "Three" etc. up to "seven". Inputs should be case-insensitive. For
	 * example, "one", "oNe", "ONE", "ONe" are all valid inputs and should produce
	 * the output Sunday
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
